extends Object


func build_header(code, header_dict):
	var header = "HTTP/1.1 " + str(code) + "\r\n"
	
	for key in header_dict:
		header += key + ": "
		if(typeof(header_dict[key]) == TYPE_ARRAY):
			
			var counter = 0
			for field in header_dict[key]:
				
				counter += 1
				if(counter < header_dict[key].size()):
					header += field + "; "
				else:
					header += field
		else:
			header += header_dict[key]
		header += "\r\n"
	
	return header
	
