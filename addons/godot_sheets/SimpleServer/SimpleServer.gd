extends Node

var _tcp_server := TCP_Server.new()

class RequestHeader:
	var verb:String
	var url:String
	var search:String
	var version:String
	var headers:Dictionary
	
	func parse(header:String) -> void:
		var lines = header.split("\r\n", false)
		var request_line = lines.pop().split(" ",true)
		
		self.verb = request_line[0]
		self.url = request_line[1]
		self.search = ""
		self.version = request_line[2]
		
		if '?' in request_line[1]:
			var query = request_line[1].split('?')
			self.url = query[0]
			self.search = query[1]
		
		self.headers = {}
		for line in lines:
			var parts = line.split(": ")
			headers[parts[0].to_lower()] = parts[1]

class ResponseHeader:
	var version:String
	var status_code:int
	var status:String
	var headers:Dictionary
	var body:String
	
	func parse(header:String) -> void:
		var lines:PoolStringArray = header.split("\r\n", false)
		var status_line:String = lines.pop()
		
		self.version = status_line.substr(0,status_line.find(" "))
		self.status = status_line.substr(status_line.find(" "),status_line.length())
		self.status_code = int(self.status.substr(0,self.status.find(" ")))
		self.body = ""
		
		while lines.size() and not lines[0] == "":
			var parts = lines.pop().split(": ")
			headers[parts[0].to_lower()] = parts[1]
		
		lines.pop()
		
		self.body = lines.join('\r\n')
		
	func _to_string():
		var response_header = PoolStringArray(["%s %s" % [self.version,self.status]])
		self.headers['content-length'] = str(self.body.to_utf8().size())
		for key in self.headers:
			response_header.append("%s: %s" % [key, self.headers.get(key)])
		response_header.append('')
		response_header.append(self.body)
		
		return response_header.join('\r\n')

func parse_query_string(query:String) -> Dictionary:
	if query.begins_with('?'):
		query = query.substr(1,query.length())
	
	var params_array = query.split("&")
	var dict = {}
	for param in params_array:
		if param != "":
			var key_value = param.split("=")
			dict[key_value[0].percent_decode()] = key_value[1].percent_decode()
	return dict

